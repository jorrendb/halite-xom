## Getting started with Halite

### 1. Install your pip/conda env
```conda env create -f environment.yml```
or if you already have an env and you want to use pip:
```pip install -r requirements.txt```


### 2. Open the notebook 01-getting-started

### 3. ????

### 4. Profit.