# UNCOMMENT ABOVE IF YOU WANT TO WRITE TO A SUBMISSION FILE
import numpy as np
import operator
# Imports helper functions
from kaggle_environments.envs.halite.helpers import *

# Directions a ship can move
directions = [ShipAction.NORTH, ShipAction.EAST, ShipAction.SOUTH, ShipAction.WEST]

# Will keep track of whether a ship is collecting halite or carrying cargo to a shipyard
ship_states = {}
occupied_positions = []
shipyard_positions = []
window_size = 3

def get_new_position(position, direction, size):
    if not direction:
        return position
    if direction == ShipAction.NORTH:
        return (position[0], position[1] + 1 % size)
    elif direction == ShipAction.EAST:
        return (position[0] + 1 % size, position[1])
    elif direction == ShipAction.SOUTH:
        return (position[0], position[1] - 1 % size)
    elif direction == ShipAction.WEST:
        return (position[0] - 1 % size, position[1])

# Returns best direction to move from one position (fromPos) to another (toPos)
# Example: If I'm at pos 0 and want to get to pos 55, which direction should I choose?
def getDirTo(fromPos, toPos, size):
    fromX, fromY = divmod(fromPos[0],size), divmod(fromPos[1],size)
    toX, toY = divmod(toPos[0],size), divmod(toPos[1],size)
    if fromY < toY: return ShipAction.NORTH
    if fromY > toY: return ShipAction.SOUTH
    if fromX < toX: return ShipAction.EAST
    if fromX > toX: return ShipAction.WEST
    

    
def safe_move(position, direction, size, direction_options=directions + [None]):
    new_position = get_new_position(position, direction, size)
    if new_position in occupied_positions:
#         print("Unsafe move. Finding new move..")
        direction_options.remove(direction)
        if direction_options:
            new_direction = np.random.choice(direction_options)
            return safe_move(position, new_direction, size, direction_options)
        else:
            return None
    else:
        return direction

    
def get_nearby_halite_ships(cell, board, size, window=window_size):
    nearby_halite = []
    nearby_ships = []
    for x in np.linspace(-(window-1)/2,(window-1)/2, window):
        for y in np.linspace(-(window-1)/2,(window-1)/2, window):
            pos_x = (cell.position[0]+x) % size
            pos_y  = (cell.position[0]+y) % size
            nearby_halite.append(board.cells[(pos_x, pos_y)].halite)
            nearby_ships.append(board.cells[(pos_x, pos_y)].ship)
    return sum(nearby_halite)/len(nearby_halite), len(nearby_ships)-nearby_ships.count(None)

def get_best_direction(position, board, size, window=window_size):
    attraction_matrix = np.zeros((window, window))
    center = (window - 1) / 2
    for i in np.linspace(-(window-1)/2,(window-1)/2, window):
        for j in np.linspace(-(window-1)/2,(window-1)/2, window):
            x = (position[0]+i) % size
            y = (position[1]+j) % size
            cell = board.cells[(x,y)]
            nearby_halite, nearby_ships = get_nearby_halite_ships(cell, board, size, window=3)
#             attraction_matrix[int(center+i)][int(center+j)] = (cell.halite + nearby_halite) / max(nearby_ships, 1)
            attraction_matrix[int(center+i)][int(center+j)] = cell.halite
    
    best_idx = np.argmax(attraction_matrix)
    best_x = best_idx % window
    best_y = best_idx // window
    direction = getDirTo((center, center), (best_x, best_y), window)
    return direction


def get_halite_hotspots(board, size, window=3):
    hotspot_dict = {}
    for x in range(size):
        for y in range(size):
            nearby_halite = []
            for i in np.linspace(-(window-1)/2,(window-1)/2, window):
                for j in np.linspace(-(window-1)/2,(window-1)/2, window):
                    window_x = (x+i) % size
                    window_y = (y+j) % size
                    nearby_halite.append(board.cells[(window_x,window_y)].halite)
            hotspot_dict[(x,y)] = sum(nearby_halite)/len(nearby_halite)
    return hotspot_dict   

def get_hotspot(position, hotspots):
    hotspot_position = max(hotspots.items(), key=operator.itemgetter(1))[0]
    distance = abs(position[0]-hotspot_position[0]) + abs(position[1]-hotspot_position[1])
    return hotspot_position, distance

def update_shipyard_positions(board):
    if shipyard_positions:
        for position in shipyard_positions:
            if not board.cells[position].shipyard:
                shipyard_positions.remove(position)

                
def get_closest_position(position, destinations):
    distances = [abs(position[0]-destination[0]) + abs(position[1]-destination[1]) for destination in destinations]
    return destinations[np.argmin(distances)], min(distances)


# Returns the commands we send to our ships and shipyards
def agent(obs, config):
    size = config.size
    board = Board(obs, config)
    me = board.current_player
#     print(shipyard_positions)
#     print(occupied_positions)
    hotspots = get_halite_hotspots(board, size, window=3)
    update_shipyard_positions(board)
    
    for shipyard in me.shipyards:
        if shipyard.position in occupied_positions and not shipyard.cell.ship:
            occupied_positions.remove(shipyard.position)
        # If there are no ships, use first shipyard to spawn a ship.
        if len(me.ships) < 15 and len(me.shipyards) > 0 and me.halite > 1000 and shipyard.position not in occupied_positions:
            shipyard.next_action = ShipyardAction.SPAWN
            occupied_positions.append(shipyard.position)

    # If there are no shipyards, convert first ship into shipyard.
    if len(me.shipyards) == 0 and len(me.ships) > 0:
        me.ships[0].next_action = ShipAction.CONVERT
        shipyard_positions.append(me.ships[0].position)
        
    
    for ship in me.ships:
#         print(f"SHIP: {ship.id}")
        if ship.position not in occupied_positions:
            occupied_positions.append(ship.position)
                
        direction = None
        if ship.next_action == None:
            
            closest_shipyard, shipyard_distance = get_closest_position(ship.position, shipyard_positions)
            hotspot_position, hotspot_distance = get_hotspot(ship.position, hotspots)
            
            
            
            
            ### Part 1: Set the ship's state 
            if ship.halite < 200: # If cargo is too low, collect halite
                ship_states[ship.id] = "COLLECT"
            if ship.halite > 500: # If cargo gets very big, deposit halite
                ship_states[ship.id] = "DEPOSIT"
                
            if (ship_states[ship.id]=='DEPOSIT' and 
                shipyard_distance > 8 and 
                hotspot_distance < 8 and 
                len(shipyard_positions)<5):
                ship.next_action = ShipAction.CONVERT
                shipyard_positions.append(ship.position)
                occupied_positions.remove(ship.position)
            
            ### Part 2: Use the ship's state to select an action
            elif ship_states[ship.id] == "COLLECT":
                # If halite at current location running low, 
                # move to the adjacent square containing the most halite
                if ship.cell.halite < 100:
                    neighbors = [ship.cell.north.halite, ship.cell.east.halite, 
                                 ship.cell.south.halite, ship.cell.west.halite]
                    best = max(range(len(neighbors)), key=neighbors.__getitem__)
                    direction = directions[best]
                    if neighbors[best]<150:
                        
                        direction = getDirTo(ship.position, hotspot_position, size)
#                         print(f"Going to hotspot at {hotspot_position} by moving {direction}")

                    direction = safe_move(ship.position, direction, size, directions + [None])
                    ship.next_action = direction
                occupied_positions.remove(ship.position)
                new_position = get_new_position(ship.position, direction, size)
                occupied_positions.append(new_position)
#                 print(f"Moving from {ship.position} to {new_position} by moving {direction}")

            
            elif ship_states[ship.id] == "DEPOSIT":
                # Move towards shipyard to deposit cargo
                direction = getDirTo(ship.position, closest_shipyard, size)
                if direction: 
                    direction = safe_move(ship.position, direction, size, directions + [None])
                    ship.next_action = direction
#                 print(f"Moving from {ship.position} to shipyard at {shipyard_positions[0]} by moving {direction}\n")
                    
                occupied_positions.remove(ship.position)
                occupied_positions.append(get_new_position(ship.position, direction, size))
    return me.next_actions



